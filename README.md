# README #

## Project and Version ##

* OnApp General Health Check
* 0.2.0

## Running the script ##

* Run with `bash healthcheck.sh`

## Who do I talk to? ##

* Paul Morris ( paul@onapp.com )
* Neal Hansen ( neal.hansen@onapp.com )

## Planned checks  ##

* ### Control Panel ###
* * 8GB RAM Minimum, recommended 16 **done**
* * RAID1 SAS Configuration **software check**
* * 100GB Disk for MySQL/Logs/etc. **done**
* * Separated networks
* * OnApp Database size **done**
* * Date/time and time zone **done**
* * * ipinfo.io mostly gives region NULL, have to use telize.com, parse json with sed :(
* * \>=1Gbit NICs (Prefer 10Gbit) **done**
* * OnApp software version **done**
* ### Hypervisors ###
* * Online, can ssh properly **done**
* * CPU's match **function to check models and MHz**
* * 100GB RAID1 **(Static only)** **done, software RAID check**
* * Redundant storage network
* * \>=1Gbit NICs (Prefer 10Gbit) **done**
* * Load vs Core count **done**
* * Zones have same Virtualization type(verify from database)
* * Kernel/Distro version **done**
* * Virtualization versions **function made**
* * OnApp Software versions **done**
* * Separated networks, redundant storage
* * Leftover tgtd targets for unused backup disks
* * Danglings dm devices
* * Average resource usage (RAM/CPU over some seconds) similar across zone **RAM usage being checked**
* ### Backups ###
* * Backup server exists and is in use **done**
* * Backup storage on separate SAN from VM's disks
* * Compare database backups vs existing backups
* * Compare database templates vs existing templates
* * Separated networks, redundant storage
* * Leftover iscsiadm connections for unused backup disks
* * Backup disks no longer required
* * \>= 1Gbit NICs (Prefer 10Gbit) **done**
* * Kernel/Distro **done**
* ### Security ###
* * Admin users have whitelists which are not 0.0.0.0/0 **done**
* * Management network behind firewall or local only
* * SSH access only from allowed IP addresses
* ### Storage ###
* * 10Gbit, FiberChannel, or Infiniband
* * Jumbo frames
* * Run ISchecker
* ### Actually make it work ###
* * Structure the checks (order: CP, Backup, Hypervisors possibly?) 
* * Add flags for only specific checks

## Possible order ##

1. ### Control Panel ###
    - a. Version
    - b. Kernel/Distro
    - c. Disk size
    - d. Date/Time
    - e. Management network hidden
    - f. \>= 1Gbps NIC
2. ### Hypervisors ###
    - * **Static**
    - a. OnApp Version
    - b. Kernel/Distro
    - c. 100GB Disk
    - d. Virtualization types match in zone
    - * **Both**
    - a. Hypervisor Types match in zone
    - b. \>=1Gbps NIC
    - c. CPU's Match in zone
    - d. Redundant storage connections
    - e. Resource usage
    - f. Date/Time
    - g. Separated networks
3. ### Backup servers ###
    - * **Static**
    - a. OnApp version 
    - b. Kernel/Distro
    - * **Both**
    - a. Date/Time
    - b. Backups on separate storage from VM disks
    - c. Management/storage networks separated
    - d. \>= 1Gbps NIC

#### Structured as ####
 * Control Panel checks **Added in checking HV/BS status**
 * For each location group, find each hypervisor zone,
 * For each hypervisor zone, find each hypervisor.
 * Check each hypervisor, report information to temporary files on CP?
 * For each location group, find each backup server (zone if it exists)
 * If zone exists, find each backup server in zone
 * Check each backup server, report information to temporary files on CP?
 * Regurgitate information from temporary files, checking for issues along the way.



## Current functions ##


```
#!cmdline
><((<((*> -$ grep -B1 -e '^function' checker.sh
# Run a SQL query
function runSQL()  
--
# Run the checks on all hypervisors and backup servers, display table. just for status
function runCheckHVandBS() 
--
# Check MySQL database size in MB
function sqlDBSize() 
--
# Check control panel version
function cpVersion()
--
# Check hypervisor version
function hvVersion()
--
# Geolocate self time zone, check with configured time zone
function timeZoneCheck() 
--
# Check HV or BS status from control server
function checkHVBSStatus() 
--
# CPU Family / Vendor checker. Information comes in CSV
function resourceCheck()
--
# load avg for 5min period compared to core count, output in CSV
function loadInfo()
--
# Free memory from xm info
function xenFreeMem()
--
# General free memory from cmd line
function freeMemGen()
--
# Detect if root disk is over 100GB, for static hypervisors and control panel
function rootDiskSize()
--
# Check NIC speeds
function netSpeed() {
--
# Check for software RAID
function checkSoftwareRAID() 
```