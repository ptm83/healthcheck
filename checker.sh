#!/bin/bash
[[ $(whoami) != "root" ]] && echo "Must be root to run this" && exit 1

# OnApp Healthcheck script to automate checks

# Initial variables
# -----------------
DBCONF="/onapp/interface/config/database.yml"
OACONF="/onapp/interface/config/on_app.yml"

# Colors
nofo='\e[0m'      #Regular
bold='\e[1m'      #Regular bold
grey='\e[30m'     #Grey
red='\e[31m'      #Red
green='\e[32m'    #Green
brown='\e[33m'    #Brown
blue='\e[34m'     #Blue
purp='\e[35m'     #Purple
cyan='\e[36m'     #Cyan
white='\e[37m'    #White
whiteb='\e[1;37m' #White Bold
lgrey='\e[1;30m'  #Light Grey
lred='\e[1;31m'   #Light Red
lgreen='\e[1;32m' #Light Green 
lbrown='\e[1;33m' #Light Brown 
lblue='\e[1;34m'  #Light Blue
lpurp='\e[1;35m'  #Light Purple
lcyan='\e[1;36m'  #Light Cyan 
###

# Database
DBDIR=`grep datadir /etc/my.cnf | cut -d'=' -f2`
DBNAME=`grep database ${DBCONF} | head -1 | awk {'print $2'} | sed "s/\"//g;s/'//g"`
SQLU=`grep username ${DBCONF} | head -1 | awk {'print $2'} | sed "s/\"//g;s/'//g"`
SQLH=`grep host ${DBCONF} | head -1 | awk {'print $2'} | sed "s/\"//g;s/'//g"`
SQLP=`grep password ${DBCONF} | head -1 | awk {'print $2'} | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g"`

# SSH Settings
SSHPORT=`grep ssh_port ${OACONF} | head -1 | awk {'print $2'}`
#SSHOPTS=`grep ssh_file_transfer_options ${OACONF} | cut -d':' -f2 | sed "s/^\s*[\"']//;s/[\"']\s*$//;"` 
#...I'm just gonna hard code the defaults...
SSHOPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p ${SSHPORT} -q"



##### Function(s) ######

### General ###
# Run a SQL query
function runSQL()  
{
    [ $# -ne 1 ] && echo "Invalid or empty SQL Query!" >&2 && exit 1
    local result=`mysql -h ${SQLH} -u ${SQLU} -p${SQLP} ${DBNAME} -Bse "${1}"`
    echo "${result}";
}

##################################################
## Most will be passed with typeset through ssh ##
##  so  need to use local variables and return  ##
##################################################

# Run the checks on all hypervisors and backup servers, display table. just for status
function runCheckHVandBS() 
{
    echo -e "Label|IP Address|Ping?|SSH?|SNMP?|Version|Kernel|Distro"
   
    # hypervisors
    local  LABELS=`runSQL "SELECT label FROM hypervisors WHERE enabled=1 AND ip_address IS NOT NULL and ip_address NOT IN (SELECT ip_address FROM backup_servers) ORDER BY id" | sed -r -e ':a;N;$!ba;s/\n/,/g'`
    local IPADDRS=`runSQL "SELECT ip_address FROM hypervisors WHERE enabled=1 AND ip_address IS NOT NULL and ip_address NOT IN (SELECT ip_address FROM backup_servers) ORDER BY id"` 
    for ip in $IPADDRS ; do
        local CURLABEL=`echo ${LABELS} | cut -d',' -f1`
        LABELS=`echo ${LABELS} | sed -r -e 's/^[^,]+,//'`
        echo -ne "${CURLABEL}|${ip}" 
        checkHVBSStatus ${ip}
    done

    # backup servers
    local  LABELS=`runSQL "SELECT label from backup_servers WHERE enabled=1 AND ip_address IS NOT NULL ORDER BY id" | sed -r -e ':a;N;$!ba;s/\n/,/g'`
    local IPADDRS=`runSQL "SELECT ip_address FROM backup_servers WHERE enabled=1 AND ip_address IS NOT NULL ORDER BY id"`
    for ip in $IPADDRS ; do
        local CURLABEL=`echo ${LABELS} | cut -d',' -f1`
        LABELS=`echo ${LABELS} | sed -r -e 's/^[^,]+,//'`
        echo -ne "${CURLABEL}|${ip}"
        checkHVBSStatus ${ip}
    done
}

# Check MySQL database size in MB
function sqlDBSize() 
{
    local DBSIZE=`du -sm ${DBDIR}/${DBNAME} | cut -f1`
    ( [[ "${DBSIZE}" > 2047 ]] && echo -e "${lred}Database is over 2GB, consider archiving stats or cleaning out the sessions table.${nofo}" ) || echo -e "${lgreen}Database under 2GB.${nofo}"
}

# Check control panel version
function cpVersion()
{
    rpm -qa onapp-cp | sed -r -e 's/onapp-cp-(.+?).noarch/\1/'
}

# Check hypervisor version
function hvVersion()
{
    cat /onapp/onapp-store-install.version
}

function xenVersion()
{
    # still needs to be reviewed
    echo -ne "`xm info | grep xen_major | cut -d: -f2`.`xm info | grep xen_minor | cut -d: -f2``xm info | grep xen_extra | cut -d: -f2`\n" | sed 's/ //g'
    rpm -qa | grep xen-4
}

# Geolocate self time zone, check with configured time zone
function timeZoneCheck() 
{
    local GEOTZ=`curl -s http://ip-api.com/json | sed -r -e 's/.+"timezone":"([^"]+?)".+/\1/g'`
    local CURTZ=`grep ZONE /etc/sysconfig/clock | cut -d'=' -f2 | tr -d '"'`
    ( [[ ${GEOTZ} != ${CURTZ} ]] && echo -e "${lred}Timezones don't seem the same, check that ${GEOTZ} = ${CURTZ}${nofo}" ) || echo -e "${lgreen}Timezones appear to match. ${cyan}${CURTZ}${nofo}"
}

# Check HV or BS status from control server
function checkHVBSStatus() 
{
    (ping ${1} -w1 2>&1 >/dev/null && echo -ne "|YES") || echo -ne "|NO"
    (su onapp -c "ssh ${SSHOPTS} root@${1} 'exit'" 2>&1 >/dev/null && echo -ne "|YES") || echo -ne "|NO"
    (nc -z ${1} 161 2>&1 >/dev/null && echo -ne "|YES" ) || echo -ne "|NO"
    echo -ne "|"`su onapp -c "ssh ${SSHOPTS} root@${1} 'cat /onapp/onapp-store-install.version 2>/dev/null'" 2>/dev/null`
    echo -ne "|"`su onapp -c "ssh ${SSHOPTS} root@${1} 'uname -r 2>/dev/null'" 2>/dev/null`
    echo -e "|"`su onapp -c "ssh ${SSHOPTS} root@${1} 'cat /etc/redhat-release 2>/dev/null'" 2>/dev/null`
}

# CPU Family / Vendor checker. Information comes in CSV
function resourceCheck()
{
    local CPUMODEL=`grep model\ name /proc/cpuinfo -m1 | cut -d':' -f2`
    local CPUSPEED=`grep cpu\ MHz /proc/cpuinfo -m1 | cut -d':' -f2 | cut -d'.' -f1 | tr -d ' '`
    local CPUCORES=`grep cpu\ cores /proc/cpuinfo -m1 | cut -d':' -f2 | tr -d ' '`
    echo "${CPUMODEL},${CPUSPEED},${CPUCORES}"
}

# load avg for 5min period compared to core count, output in CSV
function loadInfo()
{
    local LOAD=`cat /proc/loadavg | cut -d' ' -f2`
    local CORES=`grep cpu\ cores /proc/cpuinfo -m1 | cut -d':' -f2 | tr -d ' '`
    local LCRATIO=`/usr/pythoncontroller/python -c "print \"{0:.2f}\".format(1.0*${LOAD}/${CORES}*100)"`
    echo "${LOAD},${CORES},${LCRATIO}"
}

# Free memory from xm info
function xenFreeMem()
{
    /usr/pythoncontroller/python -c "print \"{0:.2f}\".format(1.0*`xm info | grep free_memory | cut -d':' -f2 | tr -d ' '`/`xm info | grep total_memory | cut -d':' -f2 | tr -d ' '`*100)"
}

# General free memory from cmd line
function freeMemGen()
{
    /usr/pythoncontroller/python -c "print \"{0:.2f}\".format(1.0*`free | grep buffers/cache | awk {'print $4'}`/`free | grep Mem | awk {'print $2'}`*100)"
}

# Detect if root disk is over 100GB, for static hypervisors and control panel
function rootDiskSize()
{
    # 100GB * 1024 * 1024 = 104857600 KB
    ( [ `df -l -P / | tail -1 | awk {'print $2'}` -ge 104857600 ] && echo -e "${lgreen}Disk over 100GB${nofo}" ) || echo -e "${lred}Disk under 100GB${nofo}"
}

# Check NIC speeds
function netSpeed() {
#    for each in $(ls -d1 /sys/devices/pci*/*/net/* | sed -r -e 's;.+/(.+?)$;\1;g') ; do
#        local ETHSPEED=`ethtool ${each} | grep Speed | awk {'print $2'}`
#        ( ( [[ ${ETHSPEED} != "1000Mb/s" ]] && [[ ${ETHSPEED} != "10Gb/s" ]] ) && echo -e "${lred}${each} link speed too low!" ) || echo -ne "${lgreen}"
#        echo -e "${each} link speed: ${ETHSPEED}${nofo}"
#    done
### ^^ old way ^^
	for each in $(find /sys/devices/pci* -type d -name net) ; do
		local CURDEV=`ls $each/ -1`
		local ETHSPEED=`cat ${each}/${CURDEV}/speed`
		echo -ne "${CURDEV} link speed "
		([[ ${ETHSPEED} < 1000 ]] && echo -ne "${lred}low! " ) || echo -ne "${lgreen}"
		echo -e "${ETHSPEED} Mbps${nofo}"
	done    
}

# Check for software RAID
function checkSoftwareRAID() 
{
    local RAIDS=`grep active /proc/mdstat`
    ( [ -z ${RAIDS} ] && echo -e "${lred}Software RAID not in use, check for hardware RAID${nofo}" ) || ( echo -e "${lgreen}Software RAID in use${nofo}"  && echo -e ${cyan}${RAIDS}${nofo} )
}


# Here is where we start all the checks. We'll begin with control panel things.

# Control Panel Version, store for later comparison.
CP_OA_VERSION=`cpVersion`
echo "OnApp Control Panel Version ${CP_OA_VERSION}."

# Kernel and distro for control server
CP_K_VERSION=`uname -r 2>/dev/null`
CP_DISTRO=`cat /etc/redhat-release 2>/dev/null`
echo "Kernel Release ${CP_K_VERSION}"
echo "Distribution: ${CP_DISTRO}"

# Check / disk size for >=100GB
rootDiskSize

checkSoftwareRAID

# Check SQL database size under 2GB (arbitrary number really. sounds nice.)
sqlDBSize

# Check time zone
timeZoneCheck

# Check admin whitelist is not empty and is not only 0.0.0.0/0
( [[ $(runSQL "SELECT count(id) FROM user_white_lists WHERE user_id=1 AND ip <> '0.0.0.0/0'") -eq 0 ]] && \
    echo -e "${lred}Admin whitelist is either empty or only contains 0.0.0.0/0!!${nofo}" ) || \
    echo -e "${lgreen}Admin whitelist is OK.${nofo}"

# network speed
netSpeed

# memory
echo `freeMemGen`"% Memory Free on Control Server."
echo

echo "Pulling table of hypervisor information..."
runCheckHVandBS | column -s '|' -t


## cleanup variables ##
unset DBCONF
unset OACONF
unset nofo
unset bold
unset grey
unset red
unset green
unset brown
unset blue
unset purp
unset cyan
unset white
unset whiteb
unset lgrey
unset lred
unset lgreen
unset lbrown
unset lblue
unset lpurp
unset lcyan
unset DBDIR
unset DBNAME
unset SQLU
unset SQLH
unset SQLP
unset SSHPORT
unset SSHOPTS
unset SSHOPTS
